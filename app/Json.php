<?php namespace App;

use Illuminate\Support\Facades\Response;

class Json {
    /**
     * Meta code 
     *
     * @var integer
     */
    public $code;

    /**
     * Message 
     *
     * @var string
     */
    public $message;

    /**
     * Data 
     *
     * @var object
     */
    public $data = NULL;

    /**
     * Return array
     *
     * @var array
     */
    public $return;


    public function makeReturn(){
        $this->return['meta']['code'] = $this->code;
        $this->return['meta']['message'] = $this->message;
        if ($this->data != NULL)
            $this->return['data'] = $this->data;
    }

    public function response(){
        self::makeReturn();
        return Response::json($this->return,$this->code);
    } 
}

?>