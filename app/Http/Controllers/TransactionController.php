<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Clients;
use App\Models\Cards;
use App\Models\Accounts;

use App\Json;

use App\Transactions\Consulta;
use App\Transactions\Retiro;
//01660123659856988745

// Lo hago en un solo archivo porque es super sencillo y da ladilla crear varios.

class TransactionController extends Controller
{

    public function __construct(){
        $this->json = new Json();
    }

    public function Consulta(Request $request){
        $balance = Accounts::find($account_id)->balance;

        $this->json->code = 200;
        $this->json->message = 'There is your balance..';
        $this->json->data = $balance;

        return $this->json->response();
    }

    public function Retiro (Request $request){
        $account = Accounts::find($request->account_id);

        
        $this->json->data['balance']['previous'] = $account->balance;


        if ($request->amount > $account->balance){
            $this->json->code = 400;
            $this->json->message = 'Your withdrawal amount cant be higher than your balance';
            return $this->json->response();
        }

        $account->balance -= $request->amount;
        $account->save();

        $this->json->code = 200;
        $this->json->message = 'All ok';
        $this->json->data['balance']['actual'] = $account->balance;
        return $this->json->response(); 
    }

    public function Deposito (Request $request){
        $account = Accounts::find($request->account_id);

        if ($request->amount <= 0){
            $this->json->code = 401;
            $this->json->message = 'Invalid transaction';
            return $this->json->response();
        }

        $this->json->data['balance']['previous'] = $account->balance;

        $account->balance += $request->amount;
        $account->save();

        $this->json->code = 200;
        $this->json->message = 'All ok!';
        $this->json->data['balance']['actual'] = $account->balance;

        return $this->json->response();
    }

    public function Transferencia (Request $request){
        $from = Accounts::find($request->from);
        $to = Clients::where('cedula','=',$request->to)->first();

        if (!count($to) > 0){
            $this->json->code = 404;
            $this->json->message = 'Destination not found';
            return $this->json->response();
        }

        $to =  $to->accounts()->first();
        
        if ($request->amount <= 0){
            $this->json->code = 400;
            $this->json->message = 'Amount must be higher than 0';
            return $this->json->response();
        }

        if ($request->amount > $from->balance){
            $this->json->code = 400;
            $this->json->message = 'Your transfer amount cant be higher than your balance';
            return $this->json->response();
        }

        $this->json->data['balance']['previous'] = $from->balance;

        $from->balance -= $request->amount;
        $from->save();

        $to->balance += $request->amount;
        $to->save();

        $this->json->code = 200;
        $this->json->message = 'All ok!';
        $this->json->data['balance']['actual'] = $from->balance;
        return $this->json->response();
    }
}
