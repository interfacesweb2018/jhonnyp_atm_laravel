<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


//models
use App\Models\Clients;
use App\Models\Cards;
use App\Models\Accounts;

//Json

use App\Json;


class SecurityController extends Controller
{

    public function __construct(){
        $this->json = new Json();
    }

    public function Authenticate(Request $request){
        $rules = array(
            'card' => 'required',
            'pin' => 'required'
        );

        $messages = array(
            'card.required' => "Card number is necessary",
            'pin.required' => "Pin number is necessary"
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()){
            $this->json->code = 501;
            $this->json->message = $validator->errors()->all()[0];
            return $this->json->response();
        }
        
        $client = Cards::where('number','=',$request->card)
                        ->where('pin','=',$request->pin)
                        ->get();
                        
        if (!count($client) > 0){
            $this->json->code = 404;
            $this->json->message = "Oops, looks like you're wrong!";
            return $this->json->response();
        }

        $account = Accounts::find($client[0]->account_id);
        $account->status = $account->status()->get();

        $client = $account->client()->get()[0];

        $this->json->code = 200;
        $this->json->message = "Welcome! :D";
        $this->json->data['client'] = $client;
        $this->json->data['account'] = $account;

        return $this->json->response();
    }

    public function destino(Request $request){
        return Clients::where('cedula','=',$request->cedula)->get()[0];
    }
}
