<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cards extends Model
{
    public function account(){
        return $this->belongsTo(Accounts::class,'account_id');
    }
}
