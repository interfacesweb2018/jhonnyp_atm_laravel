<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Accounts extends Model
{
    protected $fillable = [
        'balance'
    ];

    public $timestamps = true;

    public function client(){
        return $this->belongsTo(Clients::class,'client_id');
    }

    public function type(){
        return $this->belongsTo(AccountType::class,'accounttype_id');
    }

    public function status(){
        return $this->belongsTo(Status::class,'status_id');
    }

    public function cards(){
        return $this->belongsToMany(Cards::class,'account_id');
    }

}
