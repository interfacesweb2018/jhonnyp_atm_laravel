<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public function account(){
        return $this->belongsTo(Accounts::class,'status_id');
    }
}