<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    protected $fillable = [
        'name',
        'lastname',
        'cedula'
    ];

    protected $hidden = [
        'address',
        'birthday',
    ];

    public $timestamps = true;

    public function accounts(){
        return $this->hasMany(Accounts::class,'client_id','id');
    }




}
