<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/test', function () {
    return "it works!";
});

Route::post('/authenticate', 'SecurityController@Authenticate');

Route::get('/consulta', 'TransactionController@Consulta');
Route::post('/retiro', 'TransactionController@Retiro');
Route::post('/deposito', 'TransactionController@Deposito');
Route::post('/transferencia', 'TransactionController@Transferencia');

Route::get('/userss', 'SecurityController@destino');  // Me da ladilla hacer otro controlador asi que lo hare en securityController

// Tuve que colocar /usersss porque /user ya estaba en la api del trabajo y me daba conflicto porque estaba conectado por ssh al servidor..